SYBO Engineering case study

Time: up to 8 hours

Space Shooter base - Unity 2017.4.1f1
https://bitbucket.org/sybogames/casestudies

Overview: 
SYBO provides you a simple, modified game project based on Unity’s Space Shooter tutorial. Clone it from BitBucket at the URL above. Choose at least 4 basic questions and at least 1 advanced question below and complete them. You are encouraged to do as much as you feel comfortable with in the time allocated. 

Submission: 
Send us your completed submission as a *.zip file of your Unity 2017.4.1f1 project. Ensure you include the project subfolders of Assets, Project Settings, and PackageManager.

Please list which of the tasks you completed in this README file.


Interview: 
Be prepared to present your implementation and account for the choices made. The presentation is meant to be quick (10-15 mins) followed by a discussion.

Case Study:

Basic (Pick at least 4):

- Add 3 lives for the player
- Add a new asteroid type that splits into 2 on destruction
- Use ScriptableObjects to configure gameplay variables
- Find and fix any memory leaks in the project.
- Fix support for multiple aspect ratios
- Make UI resolution-independent
- Implement a basic start screen and results screen
- Add a new ship type which can be selected from a start screen before playing
- Add a wave system so designers can ramp up difficulty over time
- Implement object pooling for shots
- Refactor GameController to use a state machine architecture

Advanced (Pick at least 1):

- Add touch-screen controls to allow the game to be played on mobile devices
- Implement a weapon system into the game, to be able to add new weapons easily. Add a single weapon of your choice - a chance to get creative!
- Implement a boss ‘enemy ship’ that can navigate successfully, and also be affected by asteroids and has the same capabilities as the player
- Add a gameplay feature of your own choosing that you think shows off your skillset.
- Designers have also decided that they would like to be able to change game data on-the-fly, without deploying a new app. Provide them some way of being able to adjust these numbers dynamically.






